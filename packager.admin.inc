<?php

/**
 * @file
 * Administrative page callbacks for the packager module.
 */

function packager_configuration() {
  $form = array();
  
  $form['fid'] = array(
        '#type' => 'textfield',
        '#title' => t('File id'),
        '#description' => 'The id of the file to replace. Set to 0 to disable.',
        '#default_value' => variable_get('packager-fid', 0),
  );
  $form['languages'] = array(
        '#type' => 'textarea',
        '#title' => t('Languages'),
        '#description' => 'One language per line.',
        '#default_value' => implode("\n", variable_get('packager-languages', array())),
  );
  $form['modules'] = array(
        '#type' => 'textarea',
        '#title' => t('Modules'),
        '#description' => 'One module per line.',
        '#default_value' => implode("\n", variable_get('packager-modules', array())),
  );
  $form['options']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save settings'),
  );
  return $form;
}

function packager_configuration_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['fid'])) {
    form_set_error('fid', t('The file id should be numeric.'));
  }
  
  $languages = preg_split('/\s+/', $form_state['values']['languages'], -1, PREG_SPLIT_NO_EMPTY);
  $language = _packager_verify_projects($languages);
  if ($language !== TRUE) {
    form_set_error('languages', t('The language !language does not have a release.', array('!language' => $language)));
  }
  
  $modules = preg_split('/\s+/', $form_state['values']['modules'], -1, PREG_SPLIT_NO_EMPTY);
  $module = _packager_verify_projects($modules);
  if ($module !== TRUE) {
    form_set_error('modules', t('The module !module does not have a release.', array('!module' => $module)));
  }
}

function packager_configuration_submit($form, &$form_state) {
  // If the project configuration has changed we need to make sure the package will be built again.
  if (variable_get('packager-fid', 0) != $form_state['values']['fid']
        || variable_get('packager-languages', array()) != $form_state['values']['languages']
        || variable_get('packager-modules', array()) !=  $form_state['values']['modules']) {
    variable_del('packager-project-version-drupal');    
  }
  variable_set('packager-fid', (int)$form_state['values']['fid']);
  $languages = preg_split('/\s+/', $form_state['values']['languages'], -1, PREG_SPLIT_NO_EMPTY);
  variable_set('packager-languages', $languages);
  $modules = preg_split('/\s+/', $form_state['values']['modules'], -1, PREG_SPLIT_NO_EMPTY);
  variable_set('packager-modules', $modules);
  drupal_set_message(t('Your form has been saved.'));
}

/**
 * Verifies if each of the projects has a release.
 * @param $projects array
 * @return mixed TRUE if all projects have a release, the name of the project if not.
 */
function _packager_verify_projects($projects) {
  foreach ($projects as $project) {
    list ($major, $minor) = _packager_get_module_version($project);
    if (!$major) {
      return $project;
    }
  }
  return TRUE;
}
