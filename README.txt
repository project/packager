1. Introduction

The packager module can build a customized Drupal package. It does so by
downloading the latest Drupal version and adding the latest verion of the
languages and modules that are configured through the admin screen. After it
has downloaded everything it creates a new .tar.gz and replaces the uploaded
file.
